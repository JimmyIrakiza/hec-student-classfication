package domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class Combination {
	@Id@GeneratedValue
	private int id;
	@Column(unique = true)
	private String name;
	@Column(unique = true)
	private String abbreviation;
	@OneToMany(mappedBy = "applicationCombination")
	private List<Student> students;
	private boolean selected;
	@ManyToMany(mappedBy = "combinations")
	private List<School> schools;
	@OneToMany(mappedBy = "claimedCombination")
	private List<Claim> claims;
	
	public List<Claim> getClaims() {
		return claims;
	}
	public void setClaims(List<Claim> claims) {
		this.claims = claims;
	}
	public List<School> getSchools() {
		return schools;
	}
	public void setSchools(List<School> schools) {
		this.schools = schools;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	public Combination(int id, String name, List<Student> students) {
		super();
		this.id = id;
		this.name = name;
		this.students = students;
	}
	public Combination() {
		super();
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
	
}
