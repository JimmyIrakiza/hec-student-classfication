package domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;



@MappedSuperclass
public abstract class CommonDomain implements Serializable {

	private static final long serialVersionUID = -6665275582900585705L;

	public CommonDomain() {

	}

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "upDtTime")
	private Timestamp upDtTime;

	@Column(name = "updatedBy")
	private String updatedBy;

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public void setUpDtTime(Timestamp upDtTime) {
		this.upDtTime = upDtTime;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}


