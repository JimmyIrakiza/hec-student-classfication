package domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class MarksGroup {
	@Id@GeneratedValue
	private int id;
	@Column(unique = true)
	private String groupName;
	@Column(unique = true)
	private String groupSymbol;
	private int minMarks;
	private int maxMarks;
	@OneToMany(mappedBy = "marksGroup")
	private List<Student> students;
	@OneToMany(mappedBy = "division")
	private List<HECReport> reports;
	
	
	public List<HECReport> getReports() {
		return reports;
	}
	public void setReports(List<HECReport> reports) {
		this.reports = reports;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public MarksGroup() {
		super();
	}
	public int getMinMarks() {
		return minMarks;
	}
	public void setMinMarks(int minMarks) {
		this.minMarks = minMarks;
	}
	public int getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(int maxMarks) {
		this.maxMarks = maxMarks;
	}
	public String getGroupSymbol() {
		return groupSymbol;
	}
	public void setGroupSymbol(String groupSymbol) {
		this.groupSymbol = groupSymbol;
	}
	@Override
	public String toString() {
		return "MarksGroup [id=" + id + ", groupName=" + groupName + ", groupSymbol=" + groupSymbol + ", minMarks="
				+ minMarks + ", maxMarks=" + maxMarks + ", students=" + students + "]";
	}
	
	
	
}
