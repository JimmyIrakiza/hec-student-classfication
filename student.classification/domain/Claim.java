package domain;

import javax.persistence.*;

@Entity
public class Claim {
	@Id@GeneratedValue
	private int id;
	@OneToOne
	private Student student;
	@ManyToOne
	private School claimedSchool;
	@ManyToOne
	private Combination claimedCombination;
	private String claimedReason;
	private String feedback;
	private String status;
	private String studentIndex;
	
	
	
	public String getStudentIndex() {
		return studentIndex;
	}



	public void setStudentIndex(String studentIndex) {
		this.studentIndex = studentIndex;
	}



	public Claim() {
		super();
	}

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public School getClaimedSchool() {
		return claimedSchool;
	}

	public void setClaimedSchool(School claimedSchool) {
		this.claimedSchool = claimedSchool;
	}

	public Combination getClaimedCombination() {
		return claimedCombination;
	}

	public void setClaimedCombination(Combination claimedCombination) {
		this.claimedCombination = claimedCombination;
	}

	public String getClaimedReason() {
		return claimedReason;
	}

	public void setClaimedReason(String claimedReason) {
		this.claimedReason = claimedReason;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
