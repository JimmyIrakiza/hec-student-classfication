package domain;

import javax.persistence.*;

@Entity
public class HECReport {

	@Id@GeneratedValue
	private int id;
	private int biology;
	private int kinya;
	private int chemistry;
	private int maths;
	private int physics;
	private int geography;
	private int history;
	private int entrepreneurship;
	private int totalMarks;
	@ManyToOne
	private MarksGroup division;
	@OneToOne(mappedBy = "myReport")
	private Student student;

	
	public MarksGroup getDivision() {
		return division;
	}

	public void setDivision(MarksGroup division) {
		this.division = division;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBiology() {
		return biology;
	}

	public void setBiology(int biology) {
		this.biology = biology;
	}

	public int getKinya() {
		return kinya;
	}

	public void setKinya(int kinya) {
		this.kinya = kinya;
	}

	public int getChemistry() {
		return chemistry;
	}

	public void setChemistry(int chemistry) {
		this.chemistry = chemistry;
	}

	public int getMaths() {
		return maths;
	}

	public void setMaths(int maths) {
		this.maths = maths;
	}

	public int getPhysics() {
		return physics;
	}

	public void setPhysics(int physics) {
		this.physics = physics;
	}

	public int getGeography() {
		return geography;
	}

	public void setGeography(int geography) {
		this.geography = geography;
	}

	public int getHistory() {
		return history;
	}

	public void setHistory(int history) {
		this.history = history;
	}

	public int getEntrepreneurship() {
		return entrepreneurship;
	}

	public void setEntrepreneurship(int entrepreneurship) {
		this.entrepreneurship = entrepreneurship;
	}



	public HECReport(int id, int biology, int kinya, int chemistry, int maths, int physics, int geography, int history,
			int entrepreneurship, Student student) {
		super();
		this.id = id;
		this.biology = biology;
		this.kinya = kinya;
		this.chemistry = chemistry;
		this.maths = maths;
		this.physics = physics;
		this.geography = geography;
		this.history = history;
		this.entrepreneurship = entrepreneurship;
	}

	public HECReport() {
		super();
	}

	@Override
	public String toString() {
		return "HECReport [id=" + id + ", biology=" + biology + ", kinya=" + kinya + ", chemistry=" + chemistry
				+ ", maths=" + maths + ", physics=" + physics + ", geography=" + geography + ", history=" + history
				+ ", entrepreneurship=" + entrepreneurship+"]";
	}

	public int getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(int totalMarks) {
		this.totalMarks = totalMarks;
	}
	
	
}
