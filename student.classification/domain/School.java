package domain;

import javax.persistence.Column;

import java.util.List;

import javax.persistence.*;


@Entity
public class School extends CommonDomain {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id@GeneratedValue
	@Column(name = "schoolId", nullable = false, unique = true, updatable = false, insertable = false)
    private int id;
    @Column(nullable = false, unique = true)
    private String name;
    @Column(nullable = false)
    private String abbreviation;
    @ManyToOne
    @JoinColumn(name = "locationId")
    private Location location;
    @Enumerated(EnumType.STRING)
    private GenericStatus status;
    private String logo;
    private String email;
    private String telephone;
    @OneToMany(mappedBy = "mySchool")
    private List<Student> ourStudents;
    private String schoolIndexNumber;
	@OneToOne(mappedBy = "school")
    private User schoolStaff;
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "school_combinations",
            joinColumns = {@JoinColumn(name = "school_id", referencedColumnName = "schoolId")},
            inverseJoinColumns = {@JoinColumn(name = "combination_id", referencedColumnName = "ID")}
    )
	private List<Combination> combinations;
	@OneToMany(mappedBy = "claimedSchool")
    private List<Claim> claims;
	
	
	
	
	public List<Claim> getClaims() {
		return claims;
	}

	public void setClaims(List<Claim> claims) {
		this.claims = claims;
	}

	

	public List<Combination> getCombinations() {
		return combinations;
	}

	public void setCombinations(List<Combination> combinations) {
		this.combinations = combinations;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public GenericStatus getStatus() {
		return status;
	}

	public void setStatus(GenericStatus status) {
		this.status = status;
	}

	public User getSchoolStaff() {
		return schoolStaff;
	}

	public void setSchoolStaff(User schoolStaff) {
		this.schoolStaff = schoolStaff;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public List<Student> getOurStudents() {
		return ourStudents;
	}

	public void setOurStudents(List<Student> ourStudents) {
		this.ourStudents = ourStudents;
	}

	public String getSchoolIndexNumber() {
		return schoolIndexNumber;
	}

	public void setSchoolIndexNumber(String schoolIndexNumber) {
		this.schoolIndexNumber = schoolIndexNumber;
	}
	
	
}
