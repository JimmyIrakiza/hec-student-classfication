package domain;

import java.util.Date;

import javax.persistence.*;


@Entity
public class Student extends CommonDomain{
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String fname;
	private String lname;
	private String indexNumber;
	@ManyToOne
	private School mySchool;
	@OneToOne
	private HECReport myReport;
	@ManyToOne
	private MarksGroup marksGroup;
	@ManyToOne
	private School applicationSchool;
	@ManyToOne
	private Combination applicationCombination;
	@OneToOne(mappedBy = "student")
	private Claim claim;
	
	
	
	public Claim getClaim() {
		return claim;
	}
	public void setClaim(Claim claim) {
		this.claim = claim;
	}
	private String sentLetterStatus;
	
	
	
	public String getSentLetterStatus() {
		return sentLetterStatus;
	}
	public void setSentLetterStatus(String sentLetterStatus) {
		this.sentLetterStatus = sentLetterStatus;
	}
	public Combination getApplicationCombination() {
		return applicationCombination;
	}
	public void setApplicationCombination(Combination applicationCombination) {
		this.applicationCombination = applicationCombination;
	}
	public School getApplicationSchool() {
		return applicationSchool;
	}
	public void setApplicationSchool(School applicationSchool) {
		this.applicationSchool = applicationSchool;
	}
	public MarksGroup getMarksGroup() {
		return marksGroup;
	}
	public void setMarksGroup(MarksGroup marksGroup) {
		this.marksGroup = marksGroup;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public HECReport getMyReport() {
		return myReport;
	}
	public void setMyReport(HECReport myReport) {
		this.myReport = myReport;
	}
	public School getMySchool() {
		return mySchool;
	}
	public void setMySchool(School mySchool) {
		this.mySchool = mySchool;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public Student() {
		super();
	}
	public Student(String fname, String lname, String indexNumber) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.indexNumber = indexNumber;
	}
	
	
}
