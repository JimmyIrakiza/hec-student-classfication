package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IMarksGroup;
import domain.Combination;
import domain.MarksGroup;
import domain.School;

public class MarksGroupImpl extends AbstractDao<Integer, MarksGroup> implements IMarksGroup {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public MarksGroup saveMarksGroups(MarksGroup MarksGroups) {
		return saveIntable(MarksGroups);
	}

	@Override
	public List<MarksGroup> getListMarksGroups() {
		return (List<MarksGroup>) (Object) getModelList();
	}

	@Override
	public MarksGroup getMarksGroupById(int MarksGroupId, String primaryKeyclomunName) {
		return (MarksGroup) getModelById(MarksGroupId, primaryKeyclomunName);
	}

	@Override
	public MarksGroup UpdateMarksGroups(MarksGroup MarksGroups) {
		return updateIntable(MarksGroups);
		}

	@Override
	public MarksGroup getMarksGroupsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (MarksGroup) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getMarksGroupsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}
