package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IClaim;
import domain.Claim;

public class ClaimImpl extends AbstractDao<Integer, Claim> implements IClaim {

	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Claim saveClaims(Claim Claims) {
		return saveIntable(Claims);
	}

	@Override
	public List<Claim> getListClaims() {
		return (List<Claim>) (Object) getModelList();
	}

	@Override
	public Claim getClaimById(int ClaimId, String primaryKeyclomunName) {
		return (Claim) getModelById(ClaimId, primaryKeyclomunName);
	}

	@Override
	public Claim UpdateClaims(Claim Claims) {
		return updateIntable(Claims);
	}

	@Override
	public Claim getClaimsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Claim) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getClaimsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}
