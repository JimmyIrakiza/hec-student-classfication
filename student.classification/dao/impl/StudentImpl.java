package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IStudent;
import domain.Student;

public class StudentImpl extends AbstractDao<Integer, Student> implements IStudent {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

	@Override
	public Student saveStudents(Student Students) {
		return saveIntable(Students);
	}

	@Override
	public List<Student> getListStudents() {
		return (List<Student>) (Object) getModelList();
	}

	@Override
	public Student gettStudentById(int StudentId, String primaryKeyclomunName) {
		return (Student) getModelById(StudentId, primaryKeyclomunName);
	}

	@Override
	public Student UpdateStudents(Student Students) {
		return updateIntable(Students);
	}

	@Override
	public Student getStudentsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Student) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getStudentsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}
