package dao.impl;

import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IHECReport;
import domain.HECReport;
import domain.School;

public class HECReportImpl extends AbstractDao< Integer, HECReport> implements IHECReport {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

	@Override
	public HECReport saveHECReports(HECReport HECReports) {
		return saveIntable(HECReports);
	}

	@Override
	public HECReport UpdateHECReports(HECReport HECReports) {
		return updateIntable(HECReports);
	}

	@Override
	public HECReport getHECReportsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (HECReport) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getStudentReportWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}
