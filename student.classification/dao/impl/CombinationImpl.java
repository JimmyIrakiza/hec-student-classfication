package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.ICombination;
import domain.Combination;
import domain.School;

public class CombinationImpl extends AbstractDao<Integer, Combination> implements ICombination {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Combination saveCombinations(Combination Combinations) {
		return saveIntable(Combinations);
	}

	@Override
	public List<Combination> getListCombinations() {
		return (List<Combination>) (Object) getModelList();
	}

	@Override
	public Combination getCombinationById(int CombinationId, String primaryKeyclomunName) {
		return (Combination) getModelById(CombinationId, primaryKeyclomunName);
	}

	@Override
	public Combination UpdateCombinations(Combination Combinations) {
		return updateIntable(Combinations);
	}

	@Override
	public Combination getCombinationsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Combination) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getCombinationsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}


}
