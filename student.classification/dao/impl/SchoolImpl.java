package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.ISchool;
import domain.School;
import domain.User;


public class SchoolImpl extends AbstractDao<String, School> implements ISchool {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public School saveSchools(School schools) {
		return saveIntable(schools);
	}

	@Override
	public List<School> getListSchools() {
		return (List<School>) (Object) getModelList();
	}

	@Override
	public School getSchoolById(int schoolId, String primaryKeyclomunName) {
		return (School) getModelById(schoolId, primaryKeyclomunName);
	}

	@Override
	public School UpdateSchools(School Schools) {
		return updateIntable(Schools);
	}

	@Override
	public School getSchoolsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (School) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getSchoolsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}
