package dao.interfaces;

import java.util.List;

import domain.HECReport;

public interface IHECReport {
	public HECReport saveHECReports(HECReport HECReports);

	public HECReport UpdateHECReports(HECReport HECReports);

	public HECReport getHECReportsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}
