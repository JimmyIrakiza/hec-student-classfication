package dao.interfaces;

import java.util.List;

import domain.Claim;

public interface IClaim {
	public Claim saveClaims(Claim Claims);

	public List<Claim> getListClaims();

	public Claim getClaimById(int ClaimId, String primaryKeyclomunName);

	public Claim UpdateClaims(Claim Claims);

	public Claim getClaimsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}
