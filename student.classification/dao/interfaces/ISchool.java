package dao.interfaces;

import java.util.List;

import domain.School;


public interface ISchool {
	public School saveSchools(School Schools);

	public List<School> getListSchools();

	public School getSchoolById(int SchoolId, String primaryKeyclomunName);

	public School UpdateSchools(School Schools);

	public School getSchoolsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}
