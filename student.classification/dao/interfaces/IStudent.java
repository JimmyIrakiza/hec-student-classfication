package dao.interfaces;

import java.util.List;

import domain.Student;

public interface IStudent {
	public Student saveStudents(Student Students);

	public List<Student> getListStudents();

	public Student gettStudentById(int StudentId, String primaryKeyclomunName);

	public Student UpdateStudents(Student Students);

	public Student getStudentsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}
