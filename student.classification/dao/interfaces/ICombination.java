package dao.interfaces;

import java.util.List;

import domain.Combination;


public interface ICombination {
	public Combination saveCombinations(Combination Combinations);

	public List<Combination> getListCombinations();

	public Combination getCombinationById(int CombinationId, String primaryKeyclomunName);

	public Combination UpdateCombinations(Combination Combinations);

	public Combination getCombinationsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}
