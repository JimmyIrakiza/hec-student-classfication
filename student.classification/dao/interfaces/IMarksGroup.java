package dao.interfaces;

import java.util.List;

import domain.MarksGroup;


public interface IMarksGroup {
	public MarksGroup saveMarksGroups(MarksGroup MarksGroups);

	public List<MarksGroup> getListMarksGroups();

	public MarksGroup getMarksGroupById(int MarksGroupId, String primaryKeyclomunName);

	public MarksGroup UpdateMarksGroups(MarksGroup MarksGroups);

	public MarksGroup getMarksGroupsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}
