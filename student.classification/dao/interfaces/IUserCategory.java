package dao.interfaces;

import java.util.List;

import domain.UserCategory;


public interface IUserCategory {
	public UserCategory saveUsercategory(UserCategory usercategory);

	public List<UserCategory> getListUsercategory();

	public UserCategory UpdateUsercategory(UserCategory usercategory);
}
