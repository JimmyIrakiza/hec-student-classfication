package controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;

import common.JSFMessagers;
import dao.impl.ClaimImpl;
import dao.impl.CombinationImpl;
import dao.impl.HECReportImpl;
import dao.impl.LocationImpl;
import dao.impl.MarksGroupImpl;
import dao.impl.SchoolImpl;
import dao.impl.StudentImpl;
import domain.Claim;
import domain.Combination;
import domain.HECReport;
import domain.Location;
import domain.MarksGroup;
import domain.School;
import domain.Student;

@ManagedBean
@SessionScoped
public class PublicController extends HttpServlet {
	public String searchKey;
	public Student student = new Student();
	public HECReport report = new HECReport();
	private StudentImpl studentImpl = new StudentImpl();
	private HECReportImpl hecReportImpl = new HECReportImpl();
	public School school = new School();
	private SchoolImpl schoolImpl = new SchoolImpl();
	public MarksGroup marksGroup = new MarksGroup();
	private MarksGroupImpl marksGroupImpl = new MarksGroupImpl();
	private LocationImpl locationImpl = new LocationImpl();
	private CombinationImpl combinationImpl = new CombinationImpl();
	public Claim claim;
	private ClaimImpl claimImpl = new ClaimImpl();
	public List<School> schoolsList = schoolImpl.getListSchools();
	public String schoolIndexCode, combAbbrev;
	public List<Combination> combinationsList;

	
	
	public String viewMarks() {
		try {
			// searching a student based on Index Number
			if (searchKey.isBlank() || searchKey.isEmpty()) {
				JSFMessagers.addErrorMessage("Please input index number");
				return "/public/welcomePage.xhtml";
			}
			student = studentImpl.getModelWithMyHQL(new String[] { "indexNumber" }, new Object[] { searchKey },
					"from Student");
			if (student == null) {
				JSFMessagers.addErrorMessage("Student with index number " + searchKey + " not found");
				return "/public/welcomePage.xhtml";
			}
			report = student.getMyReport();
			if (report != null) {
				System.out.println("FOUND " + student.getMyReport().getId());
				report = hecReportImpl.getHECReportsWithQuery(new String[] { "id" },
						new Object[] { student.getMyReport().getId() }, "from HECReport");
				school = schoolImpl.getSchoolsWithQuery(new String[] { "id" },
						new Object[] { student.getMySchool().getId() }, "from School");
				return "/public/viewMarks.xhtml";
			}
			System.out.println("NOT FOUND " + student.getMyReport().getId());
			JSFMessagers.addErrorMessage(
					"Student with index number " + searchKey + " has no marks, please contact your HM");
			return "/public/welcomePage.xhtml";
		} catch (Exception e) {
			return "/public/viewMarks.xhtml";
		}
	}

	// Method that creates a letter to be sent to the student
	public String generateLetter(String indexNumber) throws Exception {
		Document document = new Document();
		Student student;
		School school;
		Combination combination;
		Location locSect, locDistr, locProv;
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
		try {
			student = studentImpl.getStudentsWithQuery(new String[] { "indexNumber" }, new Object[] { indexNumber },
					"from Student");

			if (student == null) {
				JSFMessagers.addErrorMessage("Student with index number not found");
				return "";
			}
			school = schoolImpl.getSchoolsWithQuery(new String[] { "id" },
					new Object[] { student.getApplicationSchool().getId() }, "from School");

			if (school == null) {
				JSFMessagers.addErrorMessage("School not found");
				return "";
			}

			combination = combinationImpl.getCombinationsWithQuery(new String[] { "id" },
					new Object[] { student.getApplicationCombination().getId() }, " from Combination");

			if (student.getSentLetterStatus() == null || !student.getSentLetterStatus().equalsIgnoreCase("Sent")) {
				JSFMessagers.addErrorMessage("The school you applied to, has not yet reacted to your result");
				return "";
			}

			locSect = locationImpl.getLocationWithQuery(new String[] { "id" },
					new Object[] { school.getLocation().getId() }, "from Location");
			locProv = locationImpl.getLocationWithQuery(new String[] { "code" },
					new Object[] { locSect.getCode().charAt(0) + "" }, "from Location");
			locDistr = locationImpl.getLocationWithQuery(new String[] { "code" },
					new Object[] { locSect.getCode().substring(0, 2) + "" }, "from Location");

			response.reset();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition",
					"attachment; filename=\"" + student.getIndexNumber() + " admission letter.pdf\"");
			PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			Paragraph header = new Paragraph("ADMISSION LETTER");
			Paragraph intro = new Paragraph("The management of " + school.getName() + " (" + school.getAbbreviation()
					+ ") is pleased to communicate to " + student.getFname() + " " + student.getLname()
					+ " that you are admited in " + student.getApplicationCombination().getName() + " ("
					+ student.getApplicationCombination().getAbbreviation() + ") in our school.");

			Paragraph body = new Paragraph("The school is located in " + locProv.getName() + ", " + locDistr.getName()
					+ " District in " + locSect.getName() + " Sector.");
			Paragraph end = new Paragraph("You're mostly welcome.");
			header.setAlignment(Element.ALIGN_CENTER);
			document.add(header);
			header.setSpacingAfter(72f);
			document.add(intro);
			intro.setSpacingAfter(20f);
			document.add(body);
			body.setSpacingAfter(20f);
			document.add(end);
			document.close();
			writer.close();
			return "/public/viewMarks.xhtml";
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String searchingSchoolCombinations() {
		try {
			School existSchool = schoolImpl.getModelWithMyHQL(new String[] { "schoolIndexNumber" },
					new Object[] { schoolIndexCode }, "from School");
			combinationsList = new ArrayList<Combination>();
			for (Combination comb : existSchool.getCombinations()) {
				combinationsList.add(comb);
			}
			return "";
		} catch (Exception e) {
			System.out.println(":::: NOT DONE " + e.getMessage() + e.getCause());
			return "";
		}
	}

	public String submitClaim() {
		try {
			
			Claim exClaim = claimImpl.getClaimsWithQuery(new String[] {"studentIndex"}, new Object[] {student.getIndexNumber()}, "from Claim");
			
			if(exClaim!=null) {
				JSFMessagers.addErrorMessage("You have another claim, wait for the response");
				return "";
			}
			School existSchool = schoolImpl.getModelWithMyHQL(new String[] { "schoolIndexNumber" },
					new Object[] { schoolIndexCode }, "from School");
			Combination combination = combinationImpl.getCombinationsWithQuery(new String[] {"abbreviation"}, new Object[] {combAbbrev}, "from Combination");
			claim.setClaimedSchool(existSchool);
			claim.setStatus("PENDING");
			claim.setStudent(student);
			claim.setStudentIndex(student.getIndexNumber());
			claim.setClaimedCombination(combination);
			claimImpl.saveClaims(claim);
			JSFMessagers.addInfoMessage("Your claim sent successfully, wait for HEC response");
			return "";
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Your Claim failed to be sent");
			return "";
		}
	}

	
	public String makeClaimInfo() {
		try {
			Claim exClaim = claimImpl.getClaimsWithQuery(new String[] {"student.id"}, new Object[] {student.getId()}, "from Claim");
			if(exClaim!=null) {
				claim = exClaim;
				JSFMessagers.addInfoMessage("Your claim has been sent successfully, wait for HEC response");
			}else {
				claim = new Claim();
				JSFMessagers.addErrorMessage("You can make your claim, fill the form");
			}
			
			return "/public/claiming.xhtml";
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "/public/claiming.xhtml";
	}
	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public HECReport getReport() {
		return report;
	}

	public void setReport(HECReport report) {
		this.report = report;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public MarksGroup getMarksGroup() {
		return marksGroup;
	}

	public void setMarksGroup(MarksGroup marksGroup) {
		this.marksGroup = marksGroup;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public List<School> getSchoolsList() {
		return schoolsList;
	}

	public void setSchoolsList(List<School> schoolsList) {
		this.schoolsList = schoolsList;
	}

	public String getSchoolIndexCode() {
		return schoolIndexCode;
	}

	public void setSchoolIndexCode(String schoolIndexCode) {
		this.schoolIndexCode = schoolIndexCode;
	}

	public String getCombAbbrev() {
		return combAbbrev;
	}

	public void setCombAbbrev(String combAbbrev) {
		this.combAbbrev = combAbbrev;
	}

	public List<Combination> getCombinationsList() {
		return combinationsList;
	}

	public void setCombinationsList(List<Combination> combinationsList) {
		this.combinationsList = combinationsList;
	}

}
