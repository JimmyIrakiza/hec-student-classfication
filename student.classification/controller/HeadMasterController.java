package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;

import common.JSFMessagers;
import dao.impl.CombinationImpl;
import dao.impl.HECReportImpl;
import dao.impl.LocationImpl;
import dao.impl.LoginImpl;
import dao.impl.SchoolImpl;
import dao.impl.StudentImpl;
import domain.Combination;
import domain.HECReport;
import domain.Location;
import domain.School;
import domain.Student;
import domain.User;
import common.DbConstant;

@ManagedBean
@SessionScoped
public class HeadMasterController {
	private StudentImpl studentImpl = new StudentImpl();
	public School school = new School();
	private SchoolImpl schoolImpl = new SchoolImpl();
	private LocationImpl locationImpl = new LocationImpl();
	private LoginImpl loginImpl = new LoginImpl();
	public List<Student> studentsList = studentImpl.getListStudents();
	public List<Location> provinces = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"PROVINCE"},
				"from Location where locationType = 'PROVINCE'");
	public List<Location> districts = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"DISTRICT"},
			"from Location where locationType = 'DISTRICT'");
	public List<Location> sectors = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"SECTOR"},
			"from Location where locationType = 'SECTOR'");
	public List<Location> cells = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"CELL"},
			"from Location where locationType = 'CELL'");
	public List<Location> villages = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"VILLAGE"},
			"from Location where locationType = 'VILLAGE'");
	String provCode, distrCode, sectCode, cellCode, villageCode;
	public Student student = new Student();
	private CombinationImpl combinationImpl = new CombinationImpl();
	public List<Combination> combinationsList;
	public String add;
	public DataModel combinationsAbbreviations;
	User activeUser;
	public List<Combination> allCombinationsList = combinationImpl.getListCombinations();
	public School existSchool = new School();
	public List<School> schoolsList = schoolImpl.getListSchools();
	public String schoolIndexCode , combAbbrev ;
	public List<Student> ourStudentsList= new ArrayList<Student>(), appliedStudentsList = new ArrayList<Student>();
	private HECReportImpl hecReportImpl = new HECReportImpl();
	public HECReport report = new HECReport();
	public List<Combination> schoolCombinationsList = new ArrayList<Combination>();
	public Combination combination;
	
	@PostConstruct
	public void init() {
		try {
			//Finding an active user for retrieving his/her school and students
			String activeUsername = LoginController.loggedInUser.getViewId();
			activeUser = loginImpl.userDetail(activeUsername);
			existSchool = schoolImpl.getModelWithMyHQL(new String[] {"createdBy"}, new Object[] {activeUser.getViewId()},
					"from School");
//			combinationsList = new ArrayList<Combination>();
//			for (Combination comb : existSchool.getCombinations()) {
//				combinationsList.add(comb);
//			}
			//List of students applied to the School and those who study at the school
			for(Student st: studentsList) {
				if(st.getCreatedBy().equals(activeUser.getViewId())) {
					ourStudentsList.add(st);
				}
				if(st.getApplicationSchool().getId()==existSchool.getId()) {
					appliedStudentsList.add(st);
				}
			}
			schoolCombinationsList = existSchool.getCombinations();
		}catch (Exception e) {
		}
	}
	
	public String registerSchool() {
		try {
			
			if(school.getName().isBlank() || school.getName().isEmpty()) {
				FacesContext context = FacesContext.getCurrentInstance();
				JSFMessagers.addErrorMessage("Failed to register, school name is required ");
				return "/templates/headMaster/createSchool.xhtml";
			} else if(school.getTelephone().isBlank() || school.getTelephone().isEmpty()) {
				JSFMessagers.addErrorMessage("Failed to register, school telephone is required ");
				return "/templates/headMaster/createSchool.xhtml";
			} else  if(school.getEmail().isBlank() || school.getEmail().isEmpty()) {
				JSFMessagers.addErrorMessage("Failed to register, school email is required ");
				return "/templates/headMaster/createSchool.xhtml";
			}
			School existSchool = schoolImpl.getModelWithMyHQL(new String[] {"createdBy"}, new Object[] {activeUser.getViewId()},
					"from School");
			if(existSchool == null || existSchool.equals(null)) {
				school.setSchoolStaff(activeUser);
				school.setCreatedBy(activeUser.getViewId());
				System.out.println("SECTOR CODE "+sectCode);
				Location location = locationImpl.getLocationWithQuery(new String[] {"code"}, new Object[] {sectCode}, "from Location");
				//Save a school
				School newSchool = schoolImpl.saveSchools(school);
				newSchool.setLocation(location);
				newSchool.setSchoolIndexNumber(location.getCode()+"S0"+newSchool.getId());
				//update a school
				schoolImpl.updateIntable(newSchool);
				//updating list of schools
				schoolsList = schoolImpl.getListSchools();
				JSFMessagers.addInfoMessage("School Registered Successfully");
				System.out.println(school.getName());
				return "/templates/headMaster/createSchool.xhtml";
				
			}else {
				JSFMessagers.addErrorMessage("You already had a school conected to your account");
				return "/templates/headMaster/createSchool.xhtml";
			}

			
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed to register: "+e.getCause());
			return "/templates/headMaster/createSchool.xhtml";
		}
	}

	
	public void provinceDitricts() {
		districts = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"DISTRICT"},
				"from Location where locationType = 'DISTRICT' AND parent.code = "+provCode);
	}
	
	public void districtSectors() {
		LocationImpl locationImpl = new LocationImpl();
		sectors = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"SECTOR"},
				"from Location where locationType = 'SECTOR' AND parent.code = "+distrCode);
	}
	
	
	
	public String registerStudent() {
		try {
			if(student.getFname().isBlank() || student.getFname().isEmpty()) {
				FacesContext context = FacesContext.getCurrentInstance();
				JSFMessagers.addErrorMessage("Failed to register, student first name is required ");
				return "/templates/headMaster/createStudent.xhtml";
			} else if(student.getLname().isBlank() || student.getLname().isEmpty()) {
				JSFMessagers.addErrorMessage("Failed to register , student last name is required ");
				return "/templates/headMaster/createStudent.xhtml";
			}
			//Finding an active user for retrieving his/her school
			School existSchool = schoolImpl.getModelWithMyHQL(new String[] {"createdBy"}, new Object[] {activeUser.getViewId()},
					"from School");
			if(existSchool==null || existSchool.equals(null)) {
				JSFMessagers.addErrorMessage("You don't represent any school in the system");
				return "/templates/headMaster/createStudent.xhtml";
			}
			if(schoolIndexCode.isBlank() || schoolIndexCode.isEmpty() || schoolIndexCode == null) {
				JSFMessagers.addErrorMessage("Select one school atleast");
				return "/templates/headMaster/createStudent.xhtml";
			}
			if(combAbbrev.isBlank() || combAbbrev.isEmpty() || combAbbrev == null) {
				JSFMessagers.addErrorMessage("Select Combination please");
				return "/templates/headMaster/createStudent.xhtml";
			}
			School appliedSchool = schoolImpl.getModelWithMyHQL(new String[] { "schoolIndexNumber" },
					new Object[] { schoolIndexCode }, "from School");
			Combination appliedCombination = combinationImpl.getModelWithMyHQL(new String[] { "abbreviation" },
					new Object[] { combAbbrev }, "from Combination");
			//setting values for student
			student.setMySchool(existSchool);
			student.setCreatedBy(activeUser.getViewId());
			student.setApplicationSchool(appliedSchool);
			student.setApplicationCombination(appliedCombination);
		//	Setting that student has not yet received letter
			student.setSentLetterStatus("notSent");
			//Saving a student
			Student newStud = studentImpl.saveStudents(student);
			newStud.setIndexNumber(existSchool.getSchoolIndexNumber()+"OL"+(100+newStud.getId()));
			//updating a student
			studentImpl.updateIntable(newStud);
			JSFMessagers.addInfoMessage("Student Registered Successfully");
			student = new Student();
			return "/templates/headMaster/createStudent.xhtml";
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed to register: " +e.getCause()+ "and "+e.getMessage());
			return "/templates/headMaster/createSchool.xhtml";
		}
	}
	
	public void submitSelections(ActionEvent event) {
	    try {
		    User activeUser = LoginController.loggedInUser;
			School existSchool = schoolImpl.getModelWithMyHQL(new String[] {"createdBy"}, new Object[] {activeUser.getViewId()},
					"from School");
			List<Combination> combinationsToAdd = new ArrayList<>();
			List<Combination> schoolsComb = existSchool.getCombinations();
		    for (Combination comb : allCombinationsList) {
		      if (comb.isSelected()) {
		    	  System.out.println("SELECTED ORG");
		    	  Combination existComb = combinationImpl.getModelWithMyHQL(new String[] {"abbreviation"}, new Object[] {comb.getAbbreviation()},
		  				"from Combination");
		    	  combinationsToAdd.add(existComb);
		      }
		    }
		    schoolsComb.addAll(combinationsToAdd);
		    existSchool.setCombinations(schoolsComb);
		    existSchool = schoolImpl.UpdateSchools(existSchool);
		    schoolCombinationsList = existSchool.getCombinations();
		    JSFMessagers.addInfoMessage("Combinations added successfully");
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Combinations failed to be added "+e.getMessage() +"&EX&"+e.getCause());
		}
	    
	  }
	
	
		public String schoolCombinations() {
			try {
				School existSchool = schoolImpl.getModelWithMyHQL(new String[] { "createdBy" },
						new Object[] { activeUser.getViewId() }, "from School");
				combinationsList = new ArrayList<Combination>();
				for (Combination comb : existSchool.getCombinations()) {
					combinationsList.add(comb);
				}
				return "/templates/headMaster/schoolCombinations.xhtml";
			} catch (Exception e) {
				System.out.println(":::: NOT DONE " + e.getMessage() + e.getCause());
				return "/templates/headMaster/schoolCombinations.xhtml";
			}
		}
		
		public String searchingSchoolCombinations() {
			try {
				School existSchool = schoolImpl.getModelWithMyHQL(new String[] { "schoolIndexNumber" },
						new Object[] { schoolIndexCode }, "from School");
				combinationsList = new ArrayList<Combination>();
				for (Combination comb : existSchool.getCombinations()) {
					combinationsList.add(comb);
				}
				return "";
			} catch (Exception e) {
				System.out.println(":::: NOT DONE " + e.getMessage() + e.getCause());
				return "";
			}
		}

		public String viewStudentMarks(String searchKey) {
			//String searchKey= "1101S05OL111";
			System.out.println("SKEY "+searchKey);
			try {
				//searching a student based on Index Number
				student = studentImpl.getModelWithMyHQL(new String[] {"indexNumber"}, new Object[] {searchKey}, "from Student");
				if(student==null) {
					JSFMessagers.addErrorMessage("Student with index number "+searchKey+ " not found");
					return "/templates/headMaster/appliedStudent.xhtml";
				}
				report = student.getMyReport();
				if(report!=null) {
					report = hecReportImpl.getHECReportsWithQuery(new String[] {"id"}, new Object[] {student.getMyReport().getId()}, "from HECReport");
					school = schoolImpl.getSchoolsWithQuery(new String[] {"id"}, new Object[] {student.getMySchool().getId()}, "from School");
					combination = student.getApplicationCombination();
					return "/templates/headMaster/viewStudentsMarks.xhtml";
				}
				JSFMessagers.addInfoMessage("Student with index number "+searchKey+ " has no marks");
				return "/templates/headMaster/appliedStudent.xhtml";
			} catch (Exception e) {
				System.out.println("NOT FOUND Ex"+e.getCause()+e.getMessage());
				return "/templates/headMaster/appliedStudent.xhtml";
			}
		}
		
		//funtion to send admission letter
		public String sendLetter(String indexNumber) {
			try {
				//searching a student based on Index Number
				student = studentImpl.getModelWithMyHQL(new String[] {"indexNumber"}, new Object[] {indexNumber}, "from Student");
				if(student==null) {
					JSFMessagers.addErrorMessage("Student with index number "+indexNumber+ " not found");
					return "/templates/headMaster/appliedStudent.xhtml";
				}
				student.setSentLetterStatus("Sent");
				studentImpl.UpdateStudents(student);
				JSFMessagers.addInfoMessage("Admission letter was sent to "+student.getFname()+"with index number "+student.getIndexNumber()+" successfully");
				return "/templates/headMaster/appliedStudent.xhtml";
			} catch (Exception e) {
				return "";
			}
		}
		
		
	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public List<Student> getStudentsList() {
		return studentsList;
	}


	public void setStudentsList(List<Student> studentsList) {
		this.studentsList = studentsList;
	}


	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<Location> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<Location> provinces) {
		this.provinces = provinces;
	}

	public List<Location> getDistricts() {
		return districts;
	}

	public void setDistricts(List<Location> districts) {
		this.districts = districts;
	}	

	public List<Location> getSectors() {
		return sectors;
	}


	public void setSectors(List<Location> sectors) {
		this.sectors = sectors;
	}


	public List<Location> getCells() {
		return cells;
	}

	public void setCells(List<Location> cells) {
		this.cells = cells;
	}

	public List<Location> getVillages() {
		return villages;
	}

	public void setVillages(List<Location> villages) {
		this.villages = villages;
	}

	public String getProvCode() {
		return provCode;
	}

	public void setProvCode(String provCode) {
		this.provCode = provCode;
	}

	public String getDistrCode() {
		return distrCode;
	}

	public void setDistrCode(String distrCode) {
		this.distrCode = distrCode;
	}

	public String getSectCode() {
		return sectCode;
	}

	public void setSectCode(String sectCode) {
		this.sectCode = sectCode;
	}

	public String getCellCode() {
		return cellCode;
	}

	public void setCellCode(String cellCode) {
		this.cellCode = cellCode;
	}

	public String getVillageCode() {
		return villageCode;
	}

	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}


	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}

	public List<Combination> getCombinationsList() {
		return combinationsList;
	}

	public void setCombinationsList(List<Combination> combinationsList) {
		this.combinationsList = combinationsList;
	}

	public DataModel getCombinationsAbbreviations() {
		return combinationsAbbreviations;
	}

	public void setCombinationsAbbreviations(DataModel combinationsAbbreviations) {
		this.combinationsAbbreviations = combinationsAbbreviations;
	}

	public List<Combination> getAllCombinationsList() {
		return allCombinationsList;
	}

	public void setAllCombinationsList(List<Combination> allCombinationsList) {
		this.allCombinationsList = allCombinationsList;
	}

	public School getExistSchool() {
		return existSchool;
	}

	public void setExistSchool(School existSchool) {
		this.existSchool = existSchool;
	}

	public List<School> getSchoolsList() {
		return schoolsList;
	}

	public void setSchoolsList(List<School> schoolsList) {
		this.schoolsList = schoolsList;
	}

	public String getSchoolIndexCode() {
		return schoolIndexCode;
	}

	public void setSchoolIndexCode(String schoolIndexCode) {
		this.schoolIndexCode = schoolIndexCode;
	}

	public String getCombAbbrev() {
		return combAbbrev;
	}

	public void setCombAbbrev(String combAbbrev) {
		this.combAbbrev = combAbbrev;
	}

	public List<Student> getOurStudentsList() {
		return ourStudentsList;
	}

	public void setOurStudentsList(List<Student> ourStudentsList) {
		this.ourStudentsList = ourStudentsList;
	}

	public List<Student> getAppliedStudentsList() {
		return appliedStudentsList;
	}

	public void setAppliedStudentsList(List<Student> appliedStudentsList) {
		this.appliedStudentsList = appliedStudentsList;
	}

	public HECReport getReport() {
		return report;
	}

	public void setReport(HECReport report) {
		this.report = report;
	}

	public List<Combination> getSchoolCombinationsList() {
		return schoolCombinationsList;
	}

	public void setSchoolCombinationsList(List<Combination> schoolCombinationsList) {
		this.schoolCombinationsList = schoolCombinationsList;
	}

	public Combination getCombination() {
		return combination;
	}

	public void setCombination(Combination combination) {
		this.combination = combination;
	}

}
