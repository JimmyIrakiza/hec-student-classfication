package controller;

import java.util.List;

import javax.faces.bean.ManagedBean;

import common.JSFMessagers;
import dao.impl.LoginImpl;
import dao.impl.UserCategoryImpl;
import dao.impl.UserImpl;
import domain.User;
import domain.UserCategory;

@ManagedBean(name = "usermanagement")
public class UserManagementController {

	public List<UserCategory> userCategories = new UserCategoryImpl().getListUsercategory();
	LoginImpl loginImpl = new LoginImpl();
	public List<User> usersList = new UserImpl().getListUsers();
	UserImpl userImpl = new UserImpl();
	UserCategoryImpl categoryImpl = new UserCategoryImpl();
	public User user = new User();
	public String email, gender;
	
	
	public String registerUser() {
		try {
			
			if(user.getFname().isBlank()||user.getFname().isEmpty()) {
				JSFMessagers.addErrorMessage("First name is required");
				return "/admin/createUser.xhtml";
			}
			
			if(user.getLname().isBlank()||user.getLname().isEmpty()) {
				JSFMessagers.addErrorMessage("Last name is required");
				return "/admin/createUser.xhtml";
			}
			
			if(user.getTelephone().isBlank()||user.getTelephone().isEmpty()) {
				JSFMessagers.addErrorMessage("Gender is required");
				return "/admin/createUser.xhtml";
			}
			
			if(gender.isBlank()||gender.isEmpty()) {
				JSFMessagers.addErrorMessage("Gender is required");
				return "/admin/createUser.xhtml";
			}
			
			if(!email.isBlank() || !email.isEmpty()) {
				UserCategory category = categoryImpl.getUserCategoryById(3, "userCatid");
				user.setUserCategory(category);
				userImpl.saveIntable(user);
				user.setGender(gender);
				user.setStatus("active");
				user.setViewId(email);
				user.setViewName(loginImpl.criptPassword(email));
				new UserImpl().saveIntable(user);
				JSFMessagers.addInfoMessage("User created created successfully");
				return "/admin/createUser.xhtml";
			}else {
				JSFMessagers.addErrorMessage("User created creation failed, email can not bu null ");
				return "/admin/createUser.xhtml";
			}
			
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("User created creation failed "+e.getMessage());
			return "/admin/createUser.xhtml";
		}
	}

	
	public String edit(int userId) {
		try {
			System.out.println(userId);
			user = userImpl.findById(userId);
			System.out.println(user.getFname());
			return "";
		} catch (Exception e) {
			return "";// TODO: handle exception
		}
	}
	
	
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}

	public List<UserCategory> getUserCategories() {
		return userCategories;
	}

	public void setUserCategories(List<UserCategory> userCategories) {
		this.userCategories = userCategories;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}

	
	
}
