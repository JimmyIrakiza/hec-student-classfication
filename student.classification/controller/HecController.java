package controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import common.JSFMessagers;
import dao.impl.ClaimImpl;
import dao.impl.CombinationImpl;
import dao.impl.HECReportImpl;
import dao.impl.MarksGroupImpl;
import dao.impl.SchoolImpl;
import dao.impl.StudentImpl;
import domain.Claim;
import domain.Combination;
import domain.HECReport;
import domain.MarksGroup;
import domain.School;
import domain.Student;

@ManagedBean
@SessionScoped
public class HecController {
	public School school = new School();
	public Student student = new Student();
	public String searchIndex;
	private StudentImpl studentImpl = new StudentImpl();
	public HECReport report = new HECReport();
	private HECReportImpl reportImpl = new HECReportImpl();
	public List<School> schoolsList;
	private SchoolImpl schoolImpl = new SchoolImpl();
	public List<Student> studentsList;
	public Combination combination = new Combination();
	private CombinationImpl combinationImpl = new CombinationImpl();
	public MarksGroup marksGroup = new MarksGroup();
	private MarksGroupImpl marksGroupImpl = new MarksGroupImpl();
	public String symbol;
	public List<Combination> combinationsList = combinationImpl.getListCombinations();
	public List<MarksGroup> marksGroupsList = marksGroupImpl.getListMarksGroups();
	private HECReportImpl hecReportImpl = new HECReportImpl();
	private ClaimImpl claimImpl = new ClaimImpl();
	public List<Claim> claimsList = claimImpl.getListClaims();
	public Claim claim = new Claim();
	public String feedback = new String();

	@PostConstruct
	public void init() {
		try {
			schoolsList = schoolImpl.getListSchools();
			studentsList = studentImpl.getListStudents();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String recordMarks() {
		try {
			if (searchIndex.isBlank() || searchIndex.isEmpty()) {
				JSFMessagers.addErrorMessage("Please input Index Number");
				return "/hec/recordingMarks.xhtml";
			}
			student = studentImpl.getStudentsWithQuery(new String[] { "indexNumber" }, new Object[] { searchIndex },
					"from Student");
			if (student == null) {
				JSFMessagers.addErrorMessage("Student with Index Number " + searchIndex + " not found");
				report = new HECReport();
				return "/templates/hec/recordingMarks.xhtml";
			} else {
				if (student.getMyReport() == null) {
					if ((report.getBiology() <= 0 || report.getBiology() > 9)
							|| (report.getChemistry() <= 0 || report.getChemistry() > 9)
							|| (report.getMaths() <= 0 || report.getMaths() > 9)
							|| (report.getEntrepreneurship() <= 0 || report.getEntrepreneurship() > 9)
							|| (report.getHistory() <= 0 || report.getHistory() > 9)
							|| (report.getGeography() <= 0 || report.getGeography() > 9)
							|| (report.getKinya() <= 0 || report.getKinya() > 9)
							|| (report.getPhysics() <= 0 || report.getPhysics() > 9)) {
						JSFMessagers.addErrorMessage("Every Subject must have values below 9 and above 0");
						return "/hec/recordingMarks.xhtml";
					} else {
						report.setTotalMarks(report.getBiology() + report.getChemistry() + report.getEntrepreneurship()
								+ report.getGeography() + report.getHistory() + report.getKinya() + report.getMaths()
								+ report.getPhysics());
						List<Object[]> divisions =  marksGroupImpl.reportList(
								"select d.id, d.groupName, d.minMarks, d.maxMarks, d.groupSymbol from MarksGroup d where d.minMarks <= "
										+ report.getTotalMarks() + " and d.maxMarks >= "+report.getTotalMarks());
						MarksGroup division = new MarksGroup();
						for(Object[] data: divisions) {
							division.setId((int) data[0]);
							division.setGroupName((String) data[1]);
							division.setMinMarks((int) data[2]);
							division.setMaxMarks((int) data[3]);
							division.setGroupSymbol((String) data[4]);
						}
						System.out.println("DIVISION  "+division);	
						report.setDivision(division);
						HECReport newReport = reportImpl.saveHECReports(report);
						student.setMyReport(newReport);
						studentImpl.updateIntable(student);
						reportImpl.updateIntable(newReport);
						marksGroupsList = marksGroupImpl.getListMarksGroups();
						marksGroup = new MarksGroup();
						JSFMessagers.addInfoMessage("Recorded Successfully");
						return "/templates/hec/recordingMarks.xhtml";
					}
				} else {
					report = student.getMyReport();
					JSFMessagers.addInfoMessage("Student with Index Number " + searchIndex + " has marks already");
					return "/templates/hec/recordingMarks.xhtml";
				}
			}
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed to record marks: " + e.getCause() + " &Ex& " + e.getMessage());
			return "/templates/hec/recordingMarks.xhtml";
		}
	}

	public String createCombination() {
		try {
			if (combination.getName().isBlank() || combination.getName().isEmpty()) {
				JSFMessagers.addErrorMessage("Combination name is required");
				return "/templates/hec/createCombination.xhtml";
			} else if (combination.getAbbreviation().isBlank() || combination.getAbbreviation().isEmpty()) {
				JSFMessagers.addErrorMessage("Combination abbreviation is required");
				return "/templates/hec/createCombination.xhtml";
			} else if (combination.getAbbreviation().length() != 3) {
				JSFMessagers.addErrorMessage("Combination abbreviation size must be 3 digits");
				return "/templates/hec/createCombination.xhtml";
			}
			Combination existComb = combinationImpl.getCombinationsWithQuery(new String[] { "abbreviation" },
					new Object[] { combination.getAbbreviation() }, "from Combination");
			if (existComb != null) {
				JSFMessagers.addErrorMessage("Combination abbreviation " + combination.getAbbreviation() + " is taken");
				return "/templates/hec/createCombination.xhtml";
			}

			existComb = combinationImpl.getCombinationsWithQuery(new String[] { "name" },
					new Object[] { combination.getName() }, "from Combination");
			if (existComb != null) {
				JSFMessagers.addErrorMessage("Combination name " + combination.getName() + " is taken");
				return "/templates/hec/createCombination.xhtml";
			}
			combinationImpl.saveCombinations(combination);
			combinationsList = combinationImpl.getListCombinations();
			combination = new Combination();
			JSFMessagers.addInfoMessage("Combination created successfully");
			return "/templates/heccreateCombination.xhtml";

		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed to create combination: " + e.getCause() + " &Ex& " + e.getMessage());
			return "/templates/hec/createCombination.xhtml";
		}
	}
	
	public String viewStudentMarks(String searchKey) {
		try {
			student = studentImpl.getModelWithMyHQL(new String[] {"indexNumber"}, new Object[] {searchKey}, "from Student");
			if(student==null) {
				JSFMessagers.addErrorMessage("Student with index number "+searchKey+ " not found");
				return "/templates/hec/studentsList.xhtml";
			}
			report = student.getMyReport();
			if(report!=null) {
				report = hecReportImpl.getHECReportsWithQuery(new String[] {"id"}, new Object[] {student.getMyReport().getId()}, "from HECReport");
				school = schoolImpl.getSchoolsWithQuery(new String[] {"id"}, new Object[] {student.getMySchool().getId()}, "from School");
				combination = student.getApplicationCombination();
				return "/templates/hec/viewStudentsMarks.xhtml";
			}
			JSFMessagers.addInfoMessage("Student with index number "+searchKey+ " has no marks");
			return "/templates/hec/studentsList.xhtml";
		} catch (Exception e) {
			System.out.println("NOT FOUND Ex"+e.getCause()+e.getMessage());
			return "/templates/hec/studentsList.xhtml";
		}
	}
	

	public String createMarksGroup() {
		try {
			if (marksGroup.getGroupName().isBlank() || marksGroup.getGroupName().isEmpty()) {
				JSFMessagers.addErrorMessage("Group name is required");
				return "/templates/hec/createMarksGroup.xhtml";
			} else if (marksGroup.getMaxMarks() <= 0 || marksGroup.getMinMarks() <= 0) {
				JSFMessagers.addErrorMessage("Marks can not be negative or zero");
				return "/templates/hec/createMarksGroup.xhtml";
			} else if (marksGroup.getMaxMarks() > 72 || marksGroup.getMinMarks() > 71) {
				JSFMessagers.addErrorMessage("Marks can not be greater to 72");
				return "/templates/hec/createMarksGroup.xhtml";
			} else if (marksGroup.getMinMarks() > marksGroup.getMaxMarks()) {
				JSFMessagers.addErrorMessage("Minimum marks can not be greater to maxmum marks");
				return "/templates/hec/createMarksGroup.xhtml";
			} else if (symbol.isBlank() || symbol.isEmpty()) {
				JSFMessagers.addErrorMessage("Please Select a symbol");
				return "/templates/hec/createMarksGroup.xhtml";
			}

			marksGroup.setGroupSymbol(symbol);
			marksGroupImpl.saveMarksGroups(marksGroup);
			marksGroupsList = marksGroupImpl.getListMarksGroups();
			JSFMessagers.addInfoMessage("Marks Group created created successfully");
			return "/templates/hec/createMarksGroup.xhtml";

		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed to create marks group: " + e.getCause() + " &Ex& " + e.getMessage());
			return "/templates/hec/createMarksGroup.xhtml";
		}
	}
	
	public String viewClaimDetails(int claimId) {
		try {
			claim = claimImpl.getClaimsWithQuery(new String[] {"id"}, new Object[] {claimId}, "from Claim");
			if(claim==null) {
				JSFMessagers.addErrorMessage("CLaim not found");
				return "/templates/hec/claimsList.xhtml";
			}
			
			return "/templates/hec/claimDetails.xhtml";
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("CLaim not found: " + e.getCause() + " &Ex& " + e.getMessage());
			return "/templates/hec/claimsList.xhtml";
		}
	}
	
	public String rejectClaim(int claimId) {
		System.out.println("ID: "+claimId);
		try {
			claim = claimImpl.getClaimsWithQuery(new String[] {"id"}, new Object[] {claimId}, "from Claim");
			if(feedback.isBlank()|| feedback.isEmpty()) {
				JSFMessagers.addErrorMessage("Please write the description of the response");
				return "/templates/hec/claimDetails.xhtml";
			}
			claim.setStatus("REJECTED");
			claim.setFeedback(feedback);
			claimImpl.UpdateClaims(claim);
			claimsList = claimImpl.getListClaims();
			JSFMessagers.addInfoMessage("CLaim Response was sent successfully");
			return "/templates/hec/claimsList.xhtml";
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed "+e.getMessage()+e.getCause());
			return "/templates/hec/claimDetails.xhtml";
		}
	}
	
	public String acceptClaim(int claimId) {
		try {
			claim = claimImpl.getClaimsWithQuery(new String[] {"id"}, new Object[] {claimId}, "from Claim");
			if(feedback.isBlank()|| feedback.isEmpty()) {
				JSFMessagers.addErrorMessage("Please write the description of the response");
				return "/templates/hec/claimDetails.xhtml";
			}
			claim.setStatus("ACCEPTED");
			claim.setFeedback(feedback);
			claimImpl.UpdateClaims(claim);
			Student student= claim.getStudent();
			student.setApplicationCombination(claim.getClaimedCombination());
			student.setApplicationSchool(claim.getClaimedSchool());
			System.out.println("CLAIMED SCHOOL "+claim.getClaimedSchool().getName());
			student.setSentLetterStatus("sent");
			studentImpl.UpdateStudents(student);
			claimsList = claimImpl.getListClaims();
			JSFMessagers.addInfoMessage("CLaim Response was sent successfully");
			return "/templates/hec/claimsList.xhtml";
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed "+e.getMessage()+e.getCause());
			return "/templates/hec/claimDetails.xhtml";
		}
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public HECReport getReport() {
		return report;
	}

	public void setReport(HECReport report) {
		this.report = report;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public List<School> getSchoolsList() {
		return schoolsList;
	}

	public void setSchoolsList(List<School> schoolsList) {
		this.schoolsList = schoolsList;
	}

	public List<Student> getStudentsList() {
		return studentsList;
	}

	public void setStudentsList(List<Student> studentsList) {
		this.studentsList = studentsList;
	}

	public Combination getCombination() {
		return combination;
	}

	public void setCombination(Combination combination) {
		this.combination = combination;
	}

	public MarksGroup getMarksGroup() {
		return marksGroup;
	}

	public void setMarksGroup(MarksGroup marksGroup) {
		this.marksGroup = marksGroup;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public List<Combination> getCombinationsList() {
		return combinationsList;
	}

	public void setCombinationsList(List<Combination> combinationsList) {
		this.combinationsList = combinationsList;
	}

	public List<MarksGroup> getMarksGroupsList() {
		return marksGroupsList;
	}

	public void setMarksGroupsList(List<MarksGroup> marksGroupsList) {
		this.marksGroupsList = marksGroupsList;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<Claim> getClaimsList() {
		return claimsList;
	}

	public void setClaimsList(List<Claim> claimsList) {
		this.claimsList = claimsList;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	
	
	
}
